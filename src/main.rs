// The Window Manager for The NeoAnt Project
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! The Window Manager for the NeoAnt Project

#![cfg_attr(not(feature = "debug"), deny(clippy::all))]
#![cfg_attr(not(feature = "debug"), forbid(clippy::nursery))]
#![cfg_attr(not(feature = "debug"), deny(clippy::pedantic))]
#![cfg_attr(not(feature = "debug"), forbid(clippy::cargo))]
#![cfg_attr(not(feature = "debug"), deny(nonstandard_style))]
#![cfg_attr(not(feature = "debug"), forbid(warnings))]
#![cfg_attr(not(feature = "debug"), forbid(rust_2018_idioms))]
#![cfg_attr(not(feature = "debug"), deny(unused))]
#![cfg_attr(not(feature = "debug"), forbid(future_incompatible))]
#![cfg_attr(not(feature = "debug"), forbid(box_pointers))]
#![cfg_attr(not(feature = "debug"), forbid(elided_lifetimes_in_paths))]
#![cfg_attr(not(feature = "debug"), forbid(missing_copy_implementations))]
#![cfg_attr(not(feature = "debug"), forbid(missing_debug_implementations))]
#![cfg_attr(not(feature = "debug"), forbid(missing_docs))]
#![cfg_attr(not(feature = "debug"), forbid(single_use_lifetimes))]
#![cfg_attr(not(feature = "debug"), forbid(trivial_casts))]
#![cfg_attr(not(feature = "debug"), forbid(trivial_numeric_casts))]
#![cfg_attr(not(feature = "debug"), forbid(unused_import_braces))]
#![cfg_attr(not(feature = "debug"), deny(unused_qualifications))]
#![cfg_attr(not(feature = "debug"), forbid(unused_results))]
#![cfg_attr(not(feature = "debug"), forbid(variant_size_differences))]
#![doc(test(attr(forbid(warnings))))]
#![doc(test(attr(forbid(clippy::all))))]
#![doc(test(attr(forbid(clippy::nursery))))]
#![doc(test(attr(forbid(clippy::pedantic))))]
#![doc(test(attr(forbid(nonstandard_style))))]
#![doc(test(attr(forbid(rust_2018_idioms))))]
#![doc(test(attr(forbid(unused))))]
#![doc(test(attr(forbid(future_incompatible))))]
#![doc(test(attr(forbid(box_pointers))))]
#![doc(test(attr(forbid(elided_lifetimes_in_paths))))]
#![doc(test(attr(forbid(missing_copy_implementations))))]
#![doc(test(attr(forbid(missing_debug_implementations))))]
#![doc(test(attr(forbid(missing_docs))))]
#![doc(test(attr(forbid(single_use_lifetimes))))]
#![doc(test(attr(forbid(trivial_casts))))]
#![doc(test(attr(forbid(trivial_numeric_casts))))]
#![doc(test(attr(forbid(unused_import_braces))))]
#![doc(test(attr(forbid(unused_qualifications))))]
#![doc(test(attr(forbid(unused_results))))]
#![doc(test(attr(forbid(variant_size_differences))))]

mod error;
mod wayland;

use error::Error;
use wayland_sys::display_run;

use log::event;
use log::Context;
use log::Level;
use log::Sink;
use log_writer::Writer;
use once_cell::sync::Lazy;
use std::io;

pub(crate) const LOGGER_ROOT: Lazy<Context<'_, Writer<io::Stderr>>> = Lazy::new(|| Context {
    name: "root",
    parent: None,
    sink: Writer::new(io::stderr()),
});

/// Entry point for the program
pub fn main() -> Result<(), Error> {
    event!(
        &LOGGER_ROOT,
        Level::Fatal,
        message = "This is a fatal error message."
    );
    event!(
        &LOGGER_ROOT,
        Level::Error,
        message = "This is an error message."
    );
    event!(&LOGGER_ROOT, Level::Warning, message = "This is a warning.");
    event!(
        &LOGGER_ROOT,
        Level::Information,
        message = "This is information."
    );
    event!(
        &LOGGER_ROOT,
        Level::Debug,
        message = "This is a debug message."
    );
    event!(&LOGGER_ROOT, Level::Trace, message = "This is a trace.");

    let compositor = wayland::types::Compositor::new()?;
    let _display = &compositor.display;
    let _socket = &compositor.socket;
    let _callback = &compositor.globals.callback;

    unsafe {
        display_run(Some(compositor.display.0));
    }

    Ok(())
}
