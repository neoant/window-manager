// Wayland Buffer Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_data_offer;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use std::os::raw::c_char;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_data_offer::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_data_offer::requests = wl_data_offer::requests {
    accept: Some(accept),
    receive: Some(receive),
    destroy: Some(destroy),
    finish: Some(finish),
    set_actions: Some(set_actions),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_offer: destroy_resource"
    );
    core::todo!();
}

extern "C" fn accept(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    serial: u32,
    mime_type: Option<NonNull<c_char>>,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_offer: accept");
    core::todo!();
}

extern "C" fn receive(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    mime_type: NonNull<c_char>,
    fd: i32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_offer: receive");
    core::todo!();
}

extern "C" fn destroy(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_offer: destroy");
    core::todo!();
}

extern "C" fn finish(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_offer: finish");
    core::todo!();
}

extern "C" fn set_actions(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    dnd_actions: u32,
    preferred_action: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_offer: set_actions"
    );
    core::todo!();
}
