// Wayland Shared Memory Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_surface;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_surface::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_surface::requests = wl_surface::requests {
    destroy: Some(destroy),
    attach: Some(attach),
    damage: Some(damage),
    frame: Some(frame),
    set_opaque_region: Some(set_opaque_region),
    set_input_region: Some(set_input_region),
    commit: Some(commit),
    set_buffer_transform: Some(set_buffer_transform),
    set_buffer_scale: Some(set_buffer_scale),
    damage_buffer: Some(damage_buffer),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: destroy_resource"
    );
    core::todo!();
}

extern "C" fn destroy(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "surface: destroy");
    core::todo!();
}

extern "C" fn attach(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    buffer: Option<NonNull<resource>>,
    x: i32,
    y: i32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "surface: attach");
    core::todo!();
}

extern "C" fn damage(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    x: i32,
    y: i32,
    width: i32,
    height: i32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "surface: attach");
    core::todo!();
}

extern "C" fn frame(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    callback: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "surface: frame");
    core::todo!();
}

extern "C" fn set_opaque_region(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    region: Option<NonNull<resource>>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: set_opaque_region"
    );
    core::todo!();
}

extern "C" fn set_input_region(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    region: Option<NonNull<resource>>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: set_input_region"
    );
    core::todo!();
}

extern "C" fn commit(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "surface: commit");
    core::todo!();
}

extern "C" fn set_buffer_transform(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    transform: i32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: set_buffer_transform"
    );
    core::todo!();
}

extern "C" fn set_buffer_scale(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    scale: i32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: set_buffer_scale"
    );
    core::todo!();
}

extern "C" fn damage_buffer(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    x: i32,
    y: i32,
    width: i32,
    height: i32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "surface: damage_buffer"
    );
    core::todo!();
}
