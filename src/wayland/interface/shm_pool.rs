// Wayland Shared Memory Pool Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_shm_pool;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_shm_pool::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_shm_pool::requests = wl_shm_pool::requests {
    create_buffer: Some(create_buffer),
    destroy: Some(destroy),
    resize: Some(resize),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shm_pool: destroy_resource"
    );
    core::todo!();
}

extern "C" fn create_buffer(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    id: u32,
    offset: i32,
    width: i32,
    height: i32,
    stride: i32,
    format: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shm_pool: create_buffer"
    );
    core::todo!();
}

extern "C" fn destroy(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "shm_pool: destroy");
    core::todo!();
}

extern "C" fn resize(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    size: i32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "shm_pool: resize");
    core::todo!();
}
