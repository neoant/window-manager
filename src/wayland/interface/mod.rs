// Wayland Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub(crate) mod buffer;
pub(crate) mod callback;
pub(crate) mod compositor;
pub(crate) mod data_device;
pub(crate) mod data_device_manager;
pub(crate) mod data_offer;
pub(crate) mod data_source;
pub(crate) mod keyboard;
pub(crate) mod pointer;
pub(crate) mod seat;
pub(crate) mod shell;
pub(crate) mod shell_surface;
pub(crate) mod shm;
pub(crate) mod shm_pool;
pub(crate) mod surface;
