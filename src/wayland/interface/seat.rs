// Wayland Shared Memory Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_seat;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_seat::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_seat::requests = wl_seat::requests {
    get_pointer: Some(get_pointer),
    get_keyboard: Some(get_keyboard),
    get_touch: Some(get_touch),
    release: Some(release),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "seat: destroy_resource"
    );
    core::todo!();
}

extern "C" fn get_pointer(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    id: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "seat: get_pointer");
    core::todo!();
}

extern "C" fn get_keyboard(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    id: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "seat: get_keyboard");
    core::todo!();
}

extern "C" fn get_touch(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    id: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "seat: get_touch");
    core::todo!();
}

extern "C" fn release(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "seat: release");
    core::todo!();
}
