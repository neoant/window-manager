// Wayland Buffer Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_data_source;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use std::os::raw::c_char;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_data_source::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_data_source::requests = wl_data_source::requests {
    offer: Some(offer),
    destroy: Some(destroy),
    set_actions: Some(set_actions),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_source: destroy_resource"
    );
    core::todo!();
}

extern "C" fn offer(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    mime_type: NonNull<c_char>,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_source: offer");
    core::todo!();
}

extern "C" fn destroy(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(&LOGGER_ROOT, Level::Trace, message = "data_source: destroy");
    core::todo!();
}

extern "C" fn set_actions(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    dnd_actions: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "data_source: set_actions"
    );
    core::todo!();
}
