// Wayland Shared Memory Interfaces
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::wayland::generated::wl_shell_surface;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use std::os::raw::c_char;
use wayland_sys::{
    client, client_post_no_memory, resource, resource_create, resource_set_implementation,
};

pub extern "C" fn global_bind(
    client: Option<NonNull<client>>,
    data: Option<NonNull<c_void>>,
    version: u32,
    id: u32,
) {
    match unsafe {
        resource_create(
            client,
            Some(NonNull::from(&wl_shell_surface::IFACE)),
            version as _,
            id,
        )
    } {
        None => {
            unsafe { client_post_no_memory(client) };
            panic!("callback_bind failed: out of memory");
        }
        Some(v) => unsafe {
            resource_set_implementation(
                Some(v),
                NonNull::new(&IMPLEMENTATION as *const _ as *const c_void as *mut c_void),
                data,
                Some(destroy_resource),
            );
        },
    }
}

static IMPLEMENTATION: wl_shell_surface::requests = wl_shell_surface::requests {
    pong: Some(pong),
    r#move: Some(r#move),
    resize: Some(resize),
    set_toplevel: Some(set_toplevel),
    set_transient: Some(set_transient),
    set_fullscreen: Some(set_fullscreen),
    set_popup: Some(set_popup),
    set_maximized: Some(set_maximized),
    set_title: Some(set_title),
    set_class: Some(set_class),
};

extern "C" fn destroy_resource(_resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: destroy_resource"
    );
    core::todo!();
}

extern "C" fn pong(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    serial: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "shell_surface: pong");
    core::todo!();
}

extern "C" fn r#move(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    seat: NonNull<resource>,
    serial: u32,
) {
    event!(&LOGGER_ROOT, Level::Trace, message = "shell_surface: move");
    core::todo!();
}

extern "C" fn resize(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    seat: NonNull<resource>,
    serial: u32,
    edges: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: resize"
    );
    core::todo!();
}

extern "C" fn set_toplevel(client: Option<NonNull<client>>, resource: Option<NonNull<resource>>) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_toplevel"
    );
    core::todo!();
}

extern "C" fn set_transient(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    parent: NonNull<resource>,
    x: i32,
    y: i32,
    flags: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_transient"
    );
    core::todo!();
}

extern "C" fn set_fullscreen(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    method: u32,
    framerate: u32,
    output: Option<NonNull<resource>>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_fullscreen"
    );
    core::todo!();
}

extern "C" fn set_popup(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    seat: NonNull<resource>,
    serial: u32,
    parent: NonNull<resource>,
    x: i32,
    y: i32,
    flags: u32,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_popup"
    );
    core::todo!();
}

extern "C" fn set_maximized(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    output: Option<NonNull<resource>>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_maximized"
    );
    core::todo!();
}

extern "C" fn set_title(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    title: NonNull<c_char>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_title"
    );
    core::todo!();
}

extern "C" fn set_class(
    client: Option<NonNull<client>>,
    resource: Option<NonNull<resource>>,
    class: NonNull<c_char>,
) {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "shell_surface: set_class"
    );
    core::todo!();
}
