// Wayland Types
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Wayland Types

use crate::error::Error;
use crate::wayland;
use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use std::env::set_var;
use std::ffi::CStr;
use std::os::raw::c_int;

use wayland_sys::{
    display, display_add_socket_auto, display_create, display_destroy, display_get_event_loop,
    event_loop_add_signal, event_source, event_source_remove, global, global_bind_func_t,
    global_create, global_destroy, interface,
};

#[derive(Debug, Copy, Clone)]
pub enum WaylandError {
    Display,
    DisplaySocket,
    GlobalResource,
}

/// Display
#[derive(Debug)]
pub(crate) struct Display(pub(crate) NonNull<display>);

impl Display {
    fn new() -> Result<Self, Error> {
        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "BEGIN: Display::new()"
        );

        let display = unsafe { display_create() };

        event!(&LOGGER_ROOT, Level::Trace, message = "END: Display::new()");

        match display {
            Some(v) => Ok(Self(v)),
            None => Err(Error::Wayland(WaylandError::Display)),
        }
    }
}

impl Default for Display {
    fn default() -> Self {
        Self::new().unwrap()
    }
}

impl Drop for Display {
    fn drop(&mut self) {
        event!(&LOGGER_ROOT, Level::Trace, message = "BEGIN: drop(Display)");

        unsafe { display_destroy(Some(self.0)) };

        event!(&LOGGER_ROOT, Level::Trace, message = "END: drop(Display)");
    }
}

#[derive(Debug)]
pub(crate) struct Socket(pub(crate) String);

impl Socket {
    fn new(display: &Display) -> Result<Self, Error> {
        event!(&LOGGER_ROOT, Level::Trace, message = "BEGIN: Socket::new()");

        let socket = unsafe { display_add_socket_auto(Some(display.0)) };
        if socket.is_none() {
            return Err(Error::Wayland(WaylandError::DisplaySocket));
        }
        let socket = unsafe { CStr::from_ptr(socket.unwrap().as_ptr()) };
        let socket = socket.to_string_lossy().into_owned();
        set_var("WAYLAND_DISPLAY", &socket);

        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = &format!("Acquired socket: {}", &socket)
        );

        event!(&LOGGER_ROOT, Level::Trace, message = "END: Socket::new()");
        Ok(Self(socket))
    }
}

/// Object for interfaces supported by the server.
pub(crate) struct Global(pub(crate) NonNull<global>);

impl Global {
    fn new(
        display: &Display,
        interface: &interface,
        data: Option<NonNull<c_void>>,
        bind: global_bind_func_t,
    ) -> Self {
        let global = unsafe {
            // UNSAFE: Foreign function
            global_create(
                Some(display.0),
                Some(NonNull::from(interface)),
                interface.version,
                data,
                bind,
            )
        };
        if global.is_none() {
            panic!("failed to setup global resource");
        }
        Self(global.unwrap())
    }
}

impl Drop for Global {
    fn drop(&mut self) {
        unsafe {
            // UNSAFE: Foreign function
            global_destroy(Some(self.0));
        }
    }
}

pub struct Globals {
    pub(crate) callback: Global,
    pub(crate) compositor: Global,
    pub(crate) shm_pool: Global,
    pub(crate) shm: Global,
    pub(crate) buffer: Global,
    pub(crate) data_offer: Global,
    pub(crate) data_source: Global,
    pub(crate) data_device: Global,
    pub(crate) data_device_manager: Global,
    pub(crate) shell: Global,
    pub(crate) shell_surface: Global,
    pub(crate) seat: Global,
    pub(crate) pointer: Global,
    pub(crate) keyboard: Global,
    pub(crate) surface: Global,
    /* pub(crate) touch: Global,
     * pub(crate) output: Global, */
}

impl Globals {
    fn new(display: &Display) -> Result<Self, Error> {
        let callback = Global::new(
            display,
            &wayland::generated::wl_callback::IFACE,
            None,
            Some(wayland::interface::callback::global_bind),
        );
        let compositor = Global::new(
            display,
            &wayland::generated::wl_compositor::IFACE,
            None,
            Some(wayland::interface::compositor::global_bind),
        );
        let shm_pool = Global::new(
            display,
            &wayland::generated::wl_shm_pool::IFACE,
            None,
            Some(wayland::interface::shm_pool::global_bind),
        );
        let shm = Global::new(
            display,
            &wayland::generated::wl_shm::IFACE,
            None,
            Some(wayland::interface::shm::global_bind),
        );
        let buffer = Global::new(
            display,
            &wayland::generated::wl_buffer::IFACE,
            None,
            Some(wayland::interface::buffer::global_bind),
        );
        let data_offer = Global::new(
            display,
            &wayland::generated::wl_data_offer::IFACE,
            None,
            Some(wayland::interface::data_offer::global_bind),
        );
        let data_source = Global::new(
            display,
            &wayland::generated::wl_data_source::IFACE,
            None,
            Some(wayland::interface::data_source::global_bind),
        );
        let data_device = Global::new(
            display,
            &wayland::generated::wl_data_device::IFACE,
            None,
            Some(wayland::interface::data_device::global_bind),
        );
        let data_device_manager = Global::new(
            display,
            &wayland::generated::wl_data_device_manager::IFACE,
            None,
            Some(wayland::interface::data_device_manager::global_bind),
        );
        let shell = Global::new(
            display,
            &wayland::generated::wl_shell::IFACE,
            None,
            Some(wayland::interface::shell::global_bind),
        );
        let shell_surface = Global::new(
            display,
            &wayland::generated::wl_shell_surface::IFACE,
            None,
            Some(wayland::interface::shell_surface::global_bind),
        );
        let seat = Global::new(
            display,
            &wayland::generated::wl_seat::IFACE,
            None,
            Some(wayland::interface::seat::global_bind),
        );
        let pointer = Global::new(
            display,
            &wayland::generated::wl_pointer::IFACE,
            None,
            Some(wayland::interface::pointer::global_bind),
        );
        let keyboard = Global::new(
            display,
            &wayland::generated::wl_keyboard::IFACE,
            None,
            Some(wayland::interface::keyboard::global_bind),
        );
        let surface = Global::new(
            display,
            &wayland::generated::wl_surface::IFACE,
            None,
            Some(wayland::interface::surface::global_bind),
        );
        Ok(Self {
            callback,
            compositor,
            shm_pool,
            shm,
            buffer,
            data_offer,
            data_source,
            data_device,
            data_device_manager,
            shell,
            shell_surface,
            seat,
            pointer,
            keyboard,
            surface,
        })
    }
}

pub(crate) struct EventSource(pub(crate) NonNull<event_source>);

impl Drop for EventSource {
    fn drop(&mut self) {
        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "BEGIN: drop(EventSource)"
        );

        let v = unsafe { event_source_remove(Some(self.0)) };

        assert!(v == 0);

        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "END: drop(EventSource)"
        );
    }
}

pub(crate) struct Compositor {
    pub(crate) event_sources: Vec<EventSource>,
    pub(crate) globals: Globals,
    pub(crate) socket: Socket,
    pub(crate) display: Display,
}

impl Compositor {
    pub(crate) fn new() -> Result<Self, Error> {
        fn handle_signal(display: &Display, signal_number: c_int) -> EventSource {
            let v = unsafe {
                // UNSAFE: Foreign function
                let event_loop = display_get_event_loop(Some(display.0));
                if event_loop.is_none() {
                    panic!("failed to retrieve event loop");
                }

                event_loop_add_signal(
                    event_loop,
                    signal_number,
                    Some(wayland::signal::handle_signal),
                    Some(display.0.cast::<c_void>()),
                )
            };
            if v.is_none() {
                panic!("failed to attach signal handler");
            }
            EventSource(v.unwrap())
        }

        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "BEGIN: Compositor::new()"
        );

        let display = Display::new()?;

        let SIGTERM = 15;
        let SIGINT = 2;
        let SIGQUIT = 3;
        let SIGCHLD = 17;

        let mut event_sources = Vec::new();

        event_sources.push(handle_signal(&display, SIGTERM));
        event_sources.push(handle_signal(&display, SIGINT));
        event_sources.push(handle_signal(&display, SIGQUIT));
        event_sources.push(handle_signal(&display, SIGCHLD));

        let socket = Socket::new(&display)?;
        let globals = Globals::new(&display)?;

        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "END: Compositor::new()"
        );
        Ok(Self {
            display,
            socket,
            globals,
            event_sources,
        })
    }
}

impl Default for Compositor {
    fn default() -> Self {
        Self::new().unwrap()
    }
}

impl Drop for Compositor {
    fn drop(&mut self) {
        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "BEGIN: drop(Compositor)"
        );
        event!(
            &LOGGER_ROOT,
            Level::Trace,
            message = "END: drop(Compositor)"
        );
    }
}
