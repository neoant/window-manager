// Wayland Bindings
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::LOGGER_ROOT;
use core::ffi::c_void;
use core::ptr::NonNull;
use log::event;
use log::Level;
use log::Sink;
use std::os::raw::c_int;
use wayland_sys::{display, display_terminate};

pub extern "C" fn handle_signal(signal_number: c_int, data: Option<NonNull<c_void>>) -> c_int {
    event!(
        &LOGGER_ROOT,
        Level::Trace,
        message = "handling signal to terminate"
    );
    let display = Some(data.unwrap().cast::<display>());
    unsafe { display_terminate(display) };
    0
}
